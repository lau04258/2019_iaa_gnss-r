import struct
from CONSTANTS import masks

class gps_nav_message:
    PARITY = 6  # each word contains 6 bit parity

    def __init__(self, space_vehicle_id, data_package):
        self.space_vehicle_id = space_vehicle_id
        self.subframe_id = (data_package[1] >> 8) & 0x07
        if self.subframe_id == 1:
            self.subframe_1 = self.parse_subframe_1(data_package)
        elif self.subframe_id == 2:
            self.subframe_2 = self.parse_subframe_2(data_package)

    def parse_subframe_1(self, data_payload):
        """
        # WN = GPS week number since latest epoch
        # TGD = Group Delay Differential (s)
        # tOC = Time of Clock (s)
        # af0 = SV Clock Bias Correction Coefficient (s)
        # af1 = SV Clock Drift Correction Coefficient (s/s)
        # af2 = Drift Rate Correction Coefficient (s/s^2)
        " each bit mask has to account for the 2-bit U-blox padding on the front (MSB) of the payload"
        """
        Wn = (data_payload[2] >> 20) & 0x3FF  # 10 bits long
        Tgd = self.unpack_signed_bits(data_payload[6], self.PARITY, masks['8_bit_signed'], (2 ** (-31)))
        toC = self.unpack_signed_bits(data_payload[7], self.PARITY, masks['16_bit_signed'], (2 ** 4))  # 16 bits long
        af0 = self.unpack_signed_bits(data_payload[9], self.PARITY + 2, masks['22_bit_signed'],
                                      (2 ** (-31)))  # 22 bits long, + 2 noninformation bits for parity computation
        af1 = self.unpack_signed_bits(data_payload[8], self.PARITY, masks['16_bit_signed'], (2 ** (-43)))
        af2 = self.unpack_signed_bits(data_payload[8], self.PARITY + 16, masks['8_bit_signed'],
                                      (2 ** (-55)))  # 8 bits long,

        return [Wn, Tgd, toC, af2, af1, af0]

    def parse_subframe_2(self, data_payload):
        """
        # IODE = Issue of Data (Ephemeris)
        # Crs = Amplitude of the Sine Harmonic Correction Term to the Orbit Radius
        # Deltan = Mean Motion Difference From Computed Value
        # M0 = Mean Anomaly at Reference Time
        # Cuc = Amplitude of the Cosine Harmonic Correction Term to the Argument of Latitude
        # e = Eccentricity
        # Cus = Amplitude of the Sine Harmonic Correction Term to the Argument of Latitude
        # sqrtA = Square Root of the Semi-Major Axis
        # tOE = Reference Time Ephemeris each bit mask has to account for the 2-bit U-blox padding on the front
        # (MSB) of the payload"
        """
        iode = self.unpack_unsigned_bits(data_payload[2], self.PARITY + 16, masks['8_bit_unsigned'])
        crs = self.unpack_signed_bits(data_payload[2], self.PARITY, masks['16_bit_signed'], 2 ** (-5))
        delta_n = self.unpack_signed_bits(data_payload[3], self.PARITY + 8, masks['16_bit_signed'], 2 ** (-43))
        # 32 bits long, over 2 words
        mean_anom = self.unpack_signed_split_bits(
            gps_nav_message.get_bits(data_payload[3], self.PARITY, masks['8_bit_signed']),
            gps_nav_message.get_bits(data_payload[4], self.PARITY, masks['24_bit_signed']),
            scale=2 ** (-31)
        )
        Cuc = self.unpack_signed_bits(data_payload[5], self.PARITY + 8, masks['16_bit_signed'], scale=2 ** (-29))
        #  32 bits long, over 2 words
        ecc = self.unpack_unsigned_split_bits(
            gps_nav_message.get_bits(data_payload[5], self.PARITY, masks['8_bit_unsigned']),
            gps_nav_message.get_bits(data_payload[6], self.PARITY, masks['24_bit_unsigned']),
            scale=2 ** (-33)
        )
        #  32 bits long, over 2 words
        Cus = self.unpack_signed_bits(data_payload[7], self.PARITY + 8, masks['16_bit_signed'], scale=2 ** (-29))
        sqrt_A = self.unpack_unsigned_split_bits(
            gps_nav_message.get_bits(data_payload[7], self.PARITY, masks['8_bit_unsigned']),
            gps_nav_message.get_bits(data_payload[8], self.PARITY, masks['24_bit_unsigned']),
            scale=2 ** (-19)
        )
        toE = self.unpack_unsigned_bits(data_payload[9], self.PARITY + 2 + 5 + 1, masks['16_bit_unsigned'], 2 ** (4))

        return [iode, crs, delta_n, mean_anom, Cuc, sqrt_A, toE]

    # def parse_subframe_3(self, data_payload):

    def unpack_signed_bits(self, word, offset, mask, scale=1):
        """
        This method unpacks signed data packages. It's been built to handle abnormal bit packages (i.e. 22 bits)
        but will also work for more standard bit packages (i.e. 16 bits), however we can use struct for that.
        :param word: the data payload
        :param offset: bit offset, shifted to the right.
        :param mask: the hex bit mask to be used
        :param scale: if the output is to be scaled, we can use this option
        :return: the scaled integer, unpacked from bits
        """
        bits = gps_nav_message.get_bits(word, offset, mask)

        # handling the signed bit
        if int(bits[0]) == 1:
            # maximum negative value we can handle
            value = 2 ** (len(bits) - 1) * (-1)
        else:
            value = 0

        # processing the rest of the bits to determine the integer value
        # removed the first bit as handled above, and creating a counter that starts from the correct power
        for idx, bit in enumerate(bits[1:], start=-len(bits) + 2):
            value += 2 ** (idx * -1) * int(bit)

        return value * scale

    @staticmethod
    def unpack_unsigned_bits(word, offset, mask, scale=1):
        """
        This method unpacks signed data packages. It's been built to handle abnormal bit packages (i.e. 22 bits)
        but will also work for more standard bit packages (i.e. 16 bits), however we can use struct for that.
        :param word: the data payload
        :param offset: bit offset, shifted to the right.
        :param mask: the hex bit mask to be used
        :param scale: if the output is to be scaled, we can use this option
        :return: the scaled integer, unpacked from bits
        """
        bits = gps_nav_message.get_bits(word, offset, mask)

        value = 0
        # processing the rest of the bits to determine the integer value
        for idx, bit in enumerate(bits, start=-len(bits) + 1):
            value += 2 ** (idx * -1) * int(bit)

        return value * scale

    @staticmethod
    def unpack_signed_split_bits(word_msb, word_lsb, scale):

        bits = word_msb + word_lsb

        # handling the signed bit
        if int(bits[0]) == 1:
            # maximum negative value we can handle
            value = 2 ** (len(bits) - 1) * (-1)
        else:
            value = 0

        # processing the rest of the bits to determine the integer value
        # removed the first bit as handled above, and creating a counter that starts from the correct power
        for idx, bit in enumerate(bits[1:], start=-len(bits) + 2):
            value += 2 ** (idx * -1) * int(bit)

        return value * scale

    @staticmethod
    def unpack_unsigned_split_bits(word_msb, word_lsb, scale):

        bits = word_msb + word_lsb

        value = 0
        # processing the rest of the bits to determine the integer value
        for idx, bit in enumerate(bits, start=-len(bits) + 1):
            value += 2 ** (idx * -1) * int(bit)

        return value * scale

    def unpack_s32s(self, word, word1):
        """Grab an signed 32 bits from weird split word, word1"""

        ubytes = bytearray(4)
        ubytes[0] = (word >> 6) & 0xff
        ubytes[1] = (word >> 14) & 0xff
        ubytes[2] = (word >> 22) & 0xff
        ubytes[3] = (word1 >> 6) & 0xff

        u = struct.unpack_from('<l', ubytes, 0)
        return u[0]

    def unpack_u32s(self, word, word1):
        """Grab an unsigned 32 bits from weird split word, word1"""

        ubytes = bytearray(4)
        ubytes[0] = (word >> 6) & 0xff
        ubytes[1] = (word >> 14) & 0xff
        ubytes[2] = (word >> 22) & 0xff
        ubytes[3] = (word1 >> 6) & 0xff

        u = struct.unpack_from('<L', ubytes, 0)
        return u[0]

    @staticmethod
    def get_bits(buffer, offset, mask):

        mask_length = len(bin(mask['hex_mask'])[2:])
        no_of_bits = len(bin((buffer >> offset))[2:])
        return bin((buffer >> offset))[2:][no_of_bits - mask_length:]

    # @staticmethod
    # def unpack_data(word, offset, mask, scale=1):
    #     """
    #     This method works for bits that are of the correct C size i.e. (1, 2, 4 and 8 bytes)
    #     :param scale:
    #     :param offset: offset to bit-shift to the right
    #     :param word: payload data
    #     :return:
    #     """
    #     array_size = int(int(mask['hex_mask']).bit_length()/8)
    #     u_bytes = bytearray(array_size)
    #     for count, byte in enumerate(u_bytes):
    #         u_bytes[count] = (word >> (offset + 8 * count)) & 0xff
    #
    #     return struct.unpack(mask['format'], u_bytes)[0]*scale