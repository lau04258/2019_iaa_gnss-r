# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 13:35:34 2020

@author: kpb18108
"""
from datetime import datetime, timedelta            
from tqdm import tqdm
import os
import csv
import numpy as np
import pandas
from sympy import Symbol, nsolve, sin, cos
from sympy.solvers import solve
from math import fabs, atan2
import scipy.optimize as opt
from scipy import linalg
from laika import raw_gnss
from mpmath import mp


#################### FUNCTIONS ################################################

def estimate_satellite_clock_bias(t, eph):
    F = -4.442807633e-10
    mu = 3.986005e14
    A = eph.sqrtA**2
    cmm = np.sqrt(mu/A**3)# % computed mean motion
    tk = t - eph.toe
    #account for beginning or end of week crossover
    if tk > 302400:
        tk = tk-604800
    
    if tk < -302400:
        tk = tk+604800
	
	# apply mean motion correction
    n = cmm + eph.dn
 
	# Mean anomaly
    mk = eph.m0 + n*tk
    
    # solve for eccentric anomaly
    E = Symbol('E') 
    Ek = float(nsolve(E - eph.e*sin(E) - mk, 0))   
    
    dsv = eph.af0 + eph.af1*(t-eph.toc) + eph.af2*(t-eph.toc)**2 + F*eph.e*eph.sqrtA*np.sin(Ek) - eph.TGD
    
    return(dsv)


def get_satellite_positionh(eph, t, compute_harmonic_correction):
    """computes the position of a satellite at time (t) given the
 	% ephemeris parameters. 
 	% Usage: [x y z] =  get_satellite_position(eph, t, compute_harmonic_correction)
 	% Input Args: eph: ephemeris data
 	%             t: time 
 	%             compute_harmonic_correction (optional): 1 if harmonic
 	%             correction should be applied, 0 if not. 
 	% Output Args: [x y z] in ECEF in meters
 	% ephmeris data must have the following fields:
 	% rcvr_tow (receiver tow)
 	% svid (satellite id)
 	% toc (reference time of clock parameters)
 	% toe (referece time of ephemeris parameters)
 	% af0, af1, af2: clock correction coefficients
 	% ura (user range accuracy)
 	% e (eccentricity)
 	% sqrtA (sqrt of semi-major axis)
 	% dn (mean motion correction)
 	% m0 (mean anomaly at reference time)
 	% w (argument of perigee)
 	% omg0 (lontitude of ascending node)
 	% i0 (inclination angle at reference time)
 	% odot (rate of right ascension)
 	% idot (rate of inclination angle)
 	% cus (argument of latitude correction, sine)
 	% cuc (argument of latitude correction, cosine)
 	% cis (inclination correction, sine)
 	% cic (inclination correction, cosine)
 	% crs (radius correction, sine)
 	% crc (radius correction, cosine)
 	% iod (issue of data number) """
 
 
    # set default value for harmonic correction
    # switch nargin
    # case 2
    compute_harmonic_correction=1
    # 
    mu = 3.986005e14 # WGS 84 value of the earth's gravitational constant for GPS user
    omega_dot_earth = 7.2921151467e-5 #(rad/sec) WGS84 value of the Earth's rotation rate
     
    # Now follow table 20-IV
    A = eph.sqrtA*eph.sqrtA
    cmm = np.sqrt(mu/A**3) # computed mean motion n_0
    tk = t - eph.toe
    # account for beginning of end of week crossover
    if (tk > 302400):
        tk = tk-604800
    
    if (tk < -302400):
        tk = tk+604800
     
    # apply mean motion correction 
    n = cmm + eph.dn
     
    # Mean anomaly radians
    mk = eph.m0 + n*tk
     
    # solve for eccentric anomaly
    E = Symbol('E')
    Ek = float(nsolve(E - eph.e*sin(E) - mk, 0))
    
    oben = (np.sqrt(1.-eph.e**2)*np.sin(Ek))/(1.-eph.e*np.cos(Ek))
    unten = (np.cos(Ek)-eph.e)/(1. - eph.e*np.cos(Ek))
    
    nu = np.arctan2(oben,unten)
    
    obenneu = eph.e + np.cos(nu)
    untenneu = 1. + eph.e * np.cos(nu)

    Ek = np.arccos(obenneu/untenneu)
     
    Phi = nu + eph.w
    # print(Phi)
    du = 0
    dr = 0
    di = 0
    
    # Second Harmon Perturbations
    du = eph.cus*np.sin(2.*Phi) + eph.cuc*np.cos(2.*Phi)
    dr = eph.crs*np.sin(2.*Phi) + eph.crc*np.cos(2.*Phi)
    di = eph.cis*np.sin(2.*Phi) + eph.cic*np.cos(2.*Phi)
    
    # Corrected Argument of Latitude 
    u = Phi + du
    # Corrected Radius
    r = A*(1.-eph.e*np.cos(Ek)) + dr   
    # print(r)
    # inclination angle at reference time in radians
    i = float(eph.i0 + eph.idot*tk + di)
    
    # Positions in Orbital Plane:
    x_prime = float(r*np.cos(u))
    y_prime = float(r*np.sin(u))
    
    # Corrected Longitude of Ascending Node
    omega = float(eph.omg0 + (eph.odot - omega_dot_earth)*tk - omega_dot_earth*eph.toe)
    
    # Earth fixed coordinates
    x = x_prime*np.cos(omega) - y_prime*np.cos(i)*np.sin(omega)
    y = x_prime*np.sin(omega) + y_prime*np.cos(i)*np.cos(omega)
    z = y_prime*np.sin(i)
    
    return(x,y,z)



# def estimate_position(xs, pr, numSat, x0, b0, dim):
    
#     # estimate_position: estimate the user's position and user clock bias
#     # Usage: [x, b, norm_dp, G] = estimate_position(xs, pr, numSat, x0, b0, dim)
#     # Input Args: xs: satellite position matrix
#     #             pr: corrected pseudo ranges (adjusted for known value of the
#     #             satellite clock bias)
#     #             numSat: number of satellites
#     #             x0: starting estimate of the user position
#     #             b0: starting point for the user clock bias
#     #             dim: dimensions of the satellite vector. 3 for 3D, 2 for 2D
#     # Notes: b and b0 are usually 0 as the current estimate of the clock bias
#     # has already been applied to the input pseudo ranges.
#     # Output Args: x: optimized user position
#     #              b: optimized user clock bias
#     #              norm_dp: normalized pseudo-range difference
#     #              G: user satellite geometry matrix, useful for computing DOPs
    
#     dx = 100*np.ones((1, dim))
#     db = 0
#     norm_dp = 100
#     numIter = 0
#     b = b0
#     dp = np.ones((numSat,1))
    
#     while np.linalg.norm(dx) > 1e-3:
#         print(np.linalg.norm(dx))
#         norms = np.sqrt(np.sum((xs-x0)**2,1))
#         #delta pseudo range:
#         for i in range(0,numSat):
#             dp[i,0] = pr[i][0] - norms[i] + b - b0
#         # dp = pr - norms + b - b0
#         G = np.array(-(xs-x0))/norms[:,None]
#         G = np.append(G, np.ones((numSat,1)),1)
#         T = np.transpose(G)
#         INV = linalg.inv(T.dot(G))
#         INVT = INV.dot(T)
#         sol = INVT.dot(dp)
#         dx = np.transpose(sol[0:dim])
#         db = sol[dim]
#         norm_dp = np.linalg.norm(dp)
#         numIter = numIter + 1
#         x0 = x0 + dx
#         b0 = b0 + db
    
#     x = x0
#     b = b0
#     return(x, b, norm_dp, G)



def estimate_position(xs, pr_all):
    # estimate_position: estimate the user's position and user clock bias
    # Usage: [x, b, norm_dp, G] = estimate_position(xs, pr, x0, b0)
    # Input Args: xs: satellite position matrix
    #             pr: corrected pseudo ranges (adjusted for known value of the
    #             satellite clock bias)
    #             numSat: number of satellites
    #             x0: starting estimate of the user position
    #             b0: starting point for the user clock bias
    #             dim: dimensions of the satellite vector. 3 for 3D, 2 for 2D
    # Notes: b and b0 are usually 0 as the current estimate of the clock bias
    # has already been applied to the input pseudo ranges.
    # Output Args: x: optimized user position
    #              b: optimized user clock bias
    #              norm_dp: normalized pseudo-range difference
    #              G: user satellite geometry matrix, useful for computing DOPs
    
    def Fx_pos(pos):
        (x, y, z, bc) = pos # The output of what we want to get
        rows = []
                     
        EARTH_ROTATION_RATE = 7.2921151467e-5
        SPEED_OF_LIGHT = 299792458      
        weight = 1
        for i in range(0,len(xs)):
            
            sat_pos = xs[i][:]
            pr = pr_all[i][0]
            theta = EARTH_ROTATION_RATE * (pr - bc) / SPEED_OF_LIGHT
            rows.append(weight * (np.sqrt(
                (sat_pos[0] * np.cos(theta) + sat_pos[1] * np.sin(theta) - x)**2 +
                (sat_pos[1] * np.cos(theta) - sat_pos[0] * np.sin(theta) - y)**2 +
                (sat_pos[2] - z)**2) - (pr - bc)))
        
        return rows 
    return Fx_pos



def WGStoEllipsoid(x,y,z):
    # WGStoEllipsoid Convert ECEF coordinates to Ellipsoidal (longitude, latitude, height above ellipsoid)
	# Usage: [lambda, phi, h] =  WGStoEllipsoid(x,y,z)
	# Input Args: coordinates in ECEF
	# Output Args: Longitude, Latitude in radians, height in meters
    
    # x=np.array(x,dtype=float)
    # y=np.array(y,dtype=float)
    # z=np.array(z,dtype=float)
    
    # a = 6378137
    # b = 6356752.3142
    # esq = 6.69437999014 * 0.001
    # e1sq = 6.73949674228 * 0.001
    # ratio = 1.0
    
    # r = np.sqrt(x * x + y * y)
    # Esq = a * a - b * b
    # F = 54 * b * b * z * z
    # G = r * r + (1 - esq) * z * z - esq * Esq
    # C = (esq * esq * F * r * r) / (pow(G, 3))
    # S = np.cbrt(1 + C + np.sqrt(C * C + 2 * C))
    # P = F / (3 * pow((S + 1 / S + 1), 2) * G * G)
    # Q = np.sqrt(1 + 2 * esq * esq * P)
    # r_0 =  -(P * esq * r) / (1 + Q) + np.sqrt(0.5 * a * a*(1 + 1.0 / Q) - \
    #       P * (1 - esq) * z * z / (Q * (1 + Q)) - 0.5 * P * r * r)
    # U = np.sqrt(pow((r - esq * r_0), 2) + z * z)
    # V = np.sqrt(pow((r - esq * r_0), 2) + (1 - esq) * z * z)
    # Z_0 = b * b * z / (a * V)
    # h = U * (1 - b * b / (a * V))
    # lat = ratio*np.arctan((z + e1sq * Z_0) / r)
    # lon = ratio*np.arctan2(y, x)
    
 	# WGS ellipsoid params
    a = 6378137
    f = 1/298.257
    e = np.sqrt(2*f-f**2)
    # From equation 4.A.3,
    lam = np.arctan2(y,x)
    p = np.sqrt(x**2+y**2)
 
    # initial value of phi assuming h = 0;
    h = 0
    phi = np.arctan2(z,(p*(1-e**2))) #4.A.5
    N = a/(1-(e*np.sin(phi))**2)**0.5    
    delta_h = 1000000
    while delta_h > 0.01:
        prev_h = h
        phi = np.arctan2(z,(p*(1-e**2*(N/(N+h))))) #4.A.5
        N = a/(1-(e*np.sin(phi))**2)**0.5
        h = p/np.cos(phi)-N
        delta_h = abs(h-prev_h)
        
    lat = phi*180./np.pi
    lon = lam*180./np.pi
        
    return(lat, lon, h)

#################### MAIN CODE ################################################


file_path = 'C:\\Users\\kpb18108\\Desktop\\IAA_GNSS\\Parsed_Data\\Parsed_raw_202001150949_rover2_BroadcastMessage_New.csv'
df = pandas.read_csv(file_path)

svid = pandas.DataFrame(df["svId2"]).to_numpy().astype(np.int64)        
t = pandas.DataFrame(df["WN"]).to_numpy().astype(np.float) #-tau
sqrtA = pandas.DataFrame(df["sqrtA"]).to_numpy().astype(np.float)
toe = pandas.DataFrame(df["tOE"]).to_numpy().astype(np.float)
dn = pandas.DataFrame(df["Deltan"]).to_numpy().astype(np.float)
m0 = pandas.DataFrame(df["M0"]).to_numpy().astype(np.float)
e = pandas.DataFrame(df["e"]).to_numpy().astype(np.float)
w = pandas.DataFrame(df["omega"]).to_numpy().astype(np.float)
cus = pandas.DataFrame(df["Cus"]).to_numpy().astype(np.float)
cuc = pandas.DataFrame(df["Cuc"]).to_numpy().astype(np.float)
crs = pandas.DataFrame(df["Crs"]).to_numpy().astype(np.float)
crc = pandas.DataFrame(df["Crc"]).to_numpy().astype(np.float)
cis = pandas.DataFrame(df["Cis"]).to_numpy().astype(np.float)
cic = pandas.DataFrame(df["Cic"]).to_numpy().astype(np.float)
i0 = pandas.DataFrame(df["i0"]).to_numpy().astype(np.float)
idot = pandas.DataFrame(df["IDOT"]).to_numpy().astype(np.float)
omg0 = pandas.DataFrame(df["Omega0"]).to_numpy().astype(np.float)
odot = pandas.DataFrame(df["Omegadot"]).to_numpy().astype(np.float)
toc = pandas.DataFrame(df["tOC"]).to_numpy().astype(np.float)
af0 = pandas.DataFrame(df["af0"]).to_numpy().astype(np.float)
af1 = pandas.DataFrame(df["af1"]).to_numpy().astype(np.float)
af2 = pandas.DataFrame(df["af2"]).to_numpy().astype(np.float)
TGD = pandas.DataFrame(df["TGD"]).to_numpy().astype(np.float)


file_path2 = 'C:\\Users\\kpb18108\\Desktop\\IAA_GNSS\\Parsed_Data\\Parsed_raw_202001150949_rover2_Measurements_New.csv'
df2 = pandas.read_csv(file_path2)

tow = pandas.DataFrame(df2["TOW"]).to_numpy().astype(np.int64)        
week = pandas.DataFrame(df2["Week"]).to_numpy().astype(np.float) #-tau
leaps = pandas.DataFrame(df2["LeapS"]).to_numpy().astype(np.float)
svIdM = pandas.DataFrame(df2["svId"]).to_numpy().astype(np.int64)
pr_raw = pandas.DataFrame(df2["prMes"]).to_numpy().astype(np.float)
Numsv = pandas.DataFrame(df2["numMeas"]).to_numpy().astype(np.float)

# initial clock bias
b = 0
pr = []
# 		% Signal transmission time
tau = 0 # for now 0
# Speed of light
c = 2.99792458e8
# Earth's rotation rate
omega_e = 7.2921151467e-5
#initial position of the user
xu = np.array([0, 0, 0])

user_position_arr = []
user_clock_bias_arr = []

class Object(object):
    pass

sv_arr = np.unique(svid)
tow_arr = np.unique(tow)
svM_arr = np.unique(svIdM)  
eph = Object()
nav = Object()

x0 = [0,0,0,0]


for i in range(0,len(tow),5):
    
    rcvr_tow = tow[i]
    
    week5 = week[5*i : 5*(i+1)] #-tau
    leaps5 = leaps[5*i : 5*(i+1)]
    svIdM5 = svIdM[5*i : 5*(i+1)]
    pr_raw5 = pr_raw[5*i : 5*(i+1)]
    Numsv5 = Numsv[5*i : 5*(i+1)]
    
    pr_corrected = []
    Xs = []
    dsv_all = []
    pr_all = []
    xs_ = []
    ys_ = []
    zs_ = []
    
    for j in range(0,5):
        
        weeks = week5[j] #-tau
        leapss = leaps5[j]
        svIdMs = svIdM5[j]
        pr_raws = pr_raw5[j]
        Numsvs = Numsv5[j]
        
        start = np.where(~np.isnan(t))
        eph_idx = np.where(np.abs(toc-rcvr_tow) == np.nanmin(np.abs(toc-rcvr_tow)))
        sv_idx = np.where(svid[eph_idx] == svIdMs[0])
        sv_idxsubframes = np.where(svid == svIdMs[0])

            
        idx = eph_idx[0][sv_idx]
        ph = idx[0]
        bli = np.where(sv_idxsubframes[0] == ph)
        fw = sv_idxsubframes[0][bli[0][0]+1]
        sw = sv_idxsubframes[0][bli[0][0]+2]
       
        if idx[0]<=start[0][0]:
            ph = idx[1]
            bli = np.where(sv_idxsubframes[0] == ph)
            fw = sv_idxsubframes[0][bli[0][0]+1]
            sw = sv_idxsubframes[0][bli[0][0]+2]
            
        # print(svIdMs[0], svid[ph], svid[fw], svid[sw])
        # print(ph, fw, sw)
        eph.t = t[idx[0],0] # time in sec
        eph.sqrtA = sqrtA[fw,0] # Square root of semi major axis [sqrt(m)]
        eph.toe = toe[fw,0] #[sec]
        eph.dn = dn[fw,0]*np.pi #[semi circle/sec] --> dn*pi = [rad/sec]
        eph.m0 = m0[fw,0]*np.pi #[semi-circles]--> m0*pi =[rad]
        eph.e = e[fw,0] #dimensionless
        eph.w = w[sw,0]*np.pi #[semi-circles] --> w*pi = [rad]
        eph.cus = cus[fw,0] #[radians]
        eph.cuc = cuc[fw,0] #[radians]
        eph.crs = crs[fw,0] #[m]
        eph.crc = crc[sw,0] #[m]
        eph.cis = cis[sw,0] #[radians]
        eph.cic = cic[sw,0] #[radians]
        eph.i0 = i0[sw,0]*np.pi # [semi-circles] --> i0*pi = [rad]
        eph.idot = idot[sw,0]*np.pi # [semi-circles/sec] --> idot*pi = [rad/sec]
        eph.omg0 = omg0[sw,0]*np.pi # [semi-circles] --> omg0*pi = [rad]
        eph.odot = odot[sw,0]*np.pi # [semi-circles/sec] --> odot*pi = [rad/sec]
        eph.toc = toc[ph,0] # [sec]
        eph.af0 = af0[ph,0] # [sec]
        eph.af1 = af1[ph,0] # [sec/sec]
        eph.af2 = af2[ph,0] # [sec^2/sec^2]
        eph.TGD = TGD[ph,0]
        
        
        dsv = estimate_satellite_clock_bias(rcvr_tow, eph) #  = deltat_sv
        pr_corrected.append(pr_raws + c*dsv)        
        cpr = pr_corrected[j] - b
        # Signal transmission time
        tau = cpr/c
        [xs ,ys, zs] = get_satellite_positionh(eph, rcvr_tow - tau, 1)
        
        xs_.append(xs)
        ys_.append(ys)
        zs_.append(zs)

        if ~np.isnan(xs):
            # Xs.append([xs,ys,zs])
            dsv_all.append(dsv)
    
    # Now lets calculate the satellite positions and construct the G
    # matrix. Then we'll run the least squares optimization to
    # calculate corrected user position and clock bias. We'll iterate
    # until change in user position and clock bias is less than a
    # threhold. In practice, the optimization converges very quickly,
    # usually in 2-3 iterations even when the starting point for the
    # user position and clock bias is far away from the true values.
    dx = 100*np.ones((1,3))
    db = 100
    while np.linalg.norm(dx) > 0.1: #and np.linalg.norm(db) > 1
        Xs = [] # concatenated satellite positions
        pr = [] # pseudoranges corrected for user clock bias
        
    
        for j in range(0,len(pr_corrected)):
            # correct for our estimate of user clock bias. Note that
            # the clock bias is in units of distance
            cpr = pr_corrected[j]
            
            # Signal transmission time
            # tau = cpr/c
            # Get satellite position
            # [xs_, ys_, zs_] = get_satellite_positionh(eph, rcvr_tow-tau, 1)
            # express satellite position in ECEF frame at time t
            # theta = omega_e*tau
            # xs_vec_ph = np.array([[np.cos(theta), np.sin(theta), 0],[-np.sin(theta), np.cos(theta), 0],[0,0,1]])
            # xs_vec = xs_vec_ph.dot([xs_[j], ys_[j], zs_[j]])
            xs_vec = np.transpose([xs_[j], ys_[j], zs_[j]])
            if ~np.isnan(xs_[j]):
                Xs.append(np.transpose(xs_vec))
                pr.append(cpr)
                
        numSV = len(Xs)
        
        # Run least squares to calculate new user position and bias
        
        Fx_pos = estimate_position(Xs, pr) 
        opt_pos = opt.least_squares(Fx_pos, x0) 
        
        # [x_, b_, norm_dp, G] = estimate_position(Xs, pr, numSV, xu, b, 3)
        # Change in the position and bias to determine when to quit
        # the iteration
        x_ = opt_pos.x[0:3]
        b_ = opt_pos.x[3]
        dx = x_ - xu
        print(np.linalg.norm(dx))
        db = b_ - b
        xu = x_
        b = b_  
        x0 =np.append(xu,b)
  
    
    # xu = opt_pos
    [lat, lon, h] = WGStoEllipsoid(xu[0], xu[1], xu[2])
        
    print(lat, lon, h)

        
        
        
        
        
