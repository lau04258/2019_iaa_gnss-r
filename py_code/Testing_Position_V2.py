# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 13:54:09 2020

@author: kpb18108
"""

from datetime import datetime, timedelta            
from tqdm import tqdm
import os
import csv
import numpy as np
import pandas
from sympy import Symbol, nsolve, sin, cos
from math import fabs, atan2
import scipy.optimize as opt
from laika import raw_gnss

# Functions

def estimate_satellite_clock_bias(t, eph):
    F = -4.442807633e-10
    mu = 3.986005e14
    A = eph.sqrtA**2
    cmm = np.sqrt(mu/A**3)# % computed mean motion
    tk = t - eph.toe
    #account for beginning or end of week crossover
    if tk > 302400:
        tk = tk-604800
    
    if tk < -302400:
        tk = tk+604800
	
	# apply mean motion correction
    n = cmm + eph.dn
 
	# Mean anomaly
    mk = eph.m0 + n*tk
    
    # solve for eccentric anomaly
    E = Symbol('E',real = True) 
    Ek = nsolve(E - eph.e*sin(E) - mk, 0)    
    
    dsv = eph.af0 + eph.af1*(t-eph.toc) + eph.af2*(t-eph.toc)**2 + F*eph.e*eph.sqrtA*np.sin(float(Ek))
    
    return(dsv)

def get_satellite_positionh(eph, t, compute_harmonic_correction):
    """computes the position of a satellite at time (t) given the
 	% ephemeris parameters. 
 	% Usage: [x y z] =  get_satellite_position(eph, t, compute_harmonic_correction)
 	% Input Args: eph: ephemeris data
 	%             t: time 
 	%             compute_harmonic_correction (optional): 1 if harmonic
 	%             correction should be applied, 0 if not. 
 	% Output Args: [x y z] in ECEF in meters
 	% ephmeris data must have the following fields:
 	% rcvr_tow (receiver tow)
 	% svid (satellite id)
 	% toc (reference time of clock parameters)
 	% toe (referece time of ephemeris parameters)
 	% af0, af1, af2: clock correction coefficients
 	% ura (user range accuracy)
 	% e (eccentricity)
 	% sqrtA (sqrt of semi-major axis)
 	% dn (mean motion correction)
 	% m0 (mean anomaly at reference time)
 	% w (argument of perigee)
 	% omg0 (lontitude of ascending node)
 	% i0 (inclination angle at reference time)
 	% odot (rate of right ascension)
 	% idot (rate of inclination angle)
 	% cus (argument of latitude correction, sine)
 	% cuc (argument of latitude correction, cosine)
 	% cis (inclination correction, sine)
 	% cic (inclination correction, cosine)
 	% crs (radius correction, sine)
 	% crc (radius correction, cosine)
 	% iod (issue of data number) """
 
 
    # set default value for harmonic correction
    # switch nargin
    # case 2
    compute_harmonic_correction=1
    # 
    mu = 3.986005e14 # WGS 84 value of the earth's gravitational constant for GPS user
    omega_dot_earth = 7.2921151467e-5 #(rad/sec) WGS84 value of the Earth's rotation rate
     
    # Now follow table 20-IV
    A = eph.sqrtA*eph.sqrtA
    cmm = np.sqrt(mu/A**3) # computed mean motion n_0
    tk = t - eph.toe
    # account for beginning of end of week crossover
    if (tk > 302400):
        tk = tk-604800
    
    if (tk < -302400):
        tk = tk+604800
     
    # apply mean motion correction
    n = cmm + eph.dn
     
    # Mean anomaly
    mk = eph.m0 + n*tk
     
    # solve for eccentric anomaly
    E = Symbol('E')
    Ek = float(nsolve(E - eph.e*sin(E), mk))
    
    nu = np.arctan(((np.sqrt(1.-eph.e**2)*np.sin(Ek))/(1.-eph.e*np.cos(Ek))) / 
                 ((np.cos(Ek)-eph.e)/(1.-eph.e*np.cos(Ek))))

    Ek = np.arccos((eph.e  + np.cos(nu))/(1+eph.e*np.cos(nu)))
     
    Phi = nu + eph.w
    du = 0
    dr = 0
    di = 0
    
    # Second Harmon Perturbations
    du = eph.cus*np.sin(2*Phi) + eph.cuc*np.cos(2*Phi)
    dr = eph.crs*np.sin(2*Phi) + eph.crc*np.cos(2*Phi)
    di = eph.cis*np.sin(2*Phi) + eph.cic*np.cos(2*Phi)
    
    # Corrected Argument of Latitude 
    u = Phi + du
    # Corrected Radius
    r = A*(1-eph.e*np.cos(Ek)) + dr     
    # inclination angle at reference time
    i = float(eph.i0 + eph.idot*tk + di)
    
    # Positions in Orbital Plane:
    x_prime = float(r*np.cos(u))
    y_prime = float(r*np.sin(u))
    
    # Corrected Longitude of Ascending Node
    omega = float(eph.omg0 + (eph.odot - omega_dot_earth)*tk - omega_dot_earth*eph.toe)
    
    # Earth fixed coordinates
    x = x_prime*np.cos(omega) - y_prime*np.cos(i)*np.sin(omega)
    y = x_prime*np.sin(omega) + y_prime*np.cos(i)*np.cos(omega)
    z = y_prime*np.sin(i)
    
    return(x,y,z)

def estimate_position(xs, pr_all, x0, b0):
    # estimate_position: estimate the user's position and user clock bias
    # Usage: [x, b, norm_dp, G] = estimate_position(xs, pr, numSat, x0, b0, dim)
    # Input Args: xs: satellite position matrix
    #             pr: corrected pseudo ranges (adjusted for known value of the
    #             satellite clock bias)
    #             numSat: number of satellites
    #             x0: starting estimate of the user position
    #             b0: starting point for the user clock bias
    #             dim: dimensions of the satellite vector. 3 for 3D, 2 for 2D
    # Notes: b and b0 are usually 0 as the current estimate of the clock bias
    # has already been applied to the input pseudo ranges.
    # Output Args: x: optimized user position
    #              b: optimized user clock bias
    #              norm_dp: normalized pseudo-range difference
    #              G: user satellite geometry matrix, useful for computing DOPs
    
    def Fx_pos(pos):
        rows = []
                     
        EARTH_ROTATION_RATE = 7.2921151467e-5
        SPEED_OF_LIGHT = 299792458      
        weight = 1
        for i in range(0,len(xs)):
            
            bc = b0[i]      
            sat_pos = xs[i][:]
            pr = pr_all[i]
            theta = EARTH_ROTATION_RATE * (pr - bc) / SPEED_OF_LIGHT
            rows.append(weight * (np.sqrt(
                  (sat_pos[0] * np.cos(theta) + sat_pos[1] * np.sin(theta) - pos[0])**2 +
                  (sat_pos[1] * np.cos(theta) - sat_pos[0] * np.sin(theta) - pos[1])**2 +
                  (sat_pos[2] - pos[2])**2) - (pr - bc)))
        
        return rows 
    return Fx_pos



def WGStoEllipsoid(x,y,z):
    # WGStoEllipsoid Convert ECEF coordinates to Ellipsoidal (longitude, latitude, height above ellipsoid)
	# Usage: [lambda, phi, h] =  WGStoEllipsoid(x,y,z)
	# Input Args: coordinates in ECEF
	# Output Args: Longitude, Latitude in radians, height in meters
    
    x=np.array(x,dtype=float)
    y=np.array(y,dtype=float)
    z=np.array(z,dtype=float)
    
	# WGS ellipsoid params
    a = 6378137
    f = 1/298.257
    e = np.sqrt(2*f-f**2)
    # From equation 4.A.3,
    lam = np.arctan2(y,x)
    p = np.sqrt(x**2+y**2)
 
    # initial value of phi assuming h = 0;
    h = 0
    phi = np.arctan2(z,(p*(1-e**2))) #4.A.5
    N = a/(1-(e*np.sin(phi))**2)**0.5    
    delta_h = 1000000
    while delta_h > 0.01:
        prev_h = h
        phi = np.arctan2(z,(p*(1-e**2*(N/(N+h))))) #4.A.5
        N = a/(1-(e*np.sin(phi))**2)**0.5
        h = p/np.cos(phi)-N
        delta_h = abs(h-prev_h)
        
    return(lam, phi, h)




file_path = 'C:\\Users\\kpb18108\\Desktop\\IAA_GNSS\\Parsed_Data\\Parsed_raw_202001150949_rover2_BroadcastMessage_New.csv'
df = pandas.read_csv(file_path)

svid = pandas.DataFrame(df["svId2"]).to_numpy().astype(np.int64)        
t = pandas.DataFrame(df["WN"]).to_numpy().astype(np.float) #-tau
sqrtA = pandas.DataFrame(df["sqrtA"]).to_numpy().astype(np.float)
toe = pandas.DataFrame(df["tOE"]).to_numpy().astype(np.float)
dn = pandas.DataFrame(df["Deltan"]).to_numpy().astype(np.float)
m0 = pandas.DataFrame(df["M0"]).to_numpy().astype(np.float)
e = pandas.DataFrame(df["e"]).to_numpy().astype(np.float)
w = pandas.DataFrame(df["omega"]).to_numpy().astype(np.float)
cus = pandas.DataFrame(df["Cus"]).to_numpy().astype(np.float)
cuc = pandas.DataFrame(df["Cuc"]).to_numpy().astype(np.float)
crs = pandas.DataFrame(df["Crs"]).to_numpy().astype(np.float)
crc = pandas.DataFrame(df["Crc"]).to_numpy().astype(np.float)
cis = pandas.DataFrame(df["Cis"]).to_numpy().astype(np.float)
cic = pandas.DataFrame(df["Cic"]).to_numpy().astype(np.float)
i0 = pandas.DataFrame(df["i0"]).to_numpy().astype(np.float)
idot = pandas.DataFrame(df["IDOT"]).to_numpy().astype(np.float)
omg0 = pandas.DataFrame(df["Omega0"]).to_numpy().astype(np.float)
odot = pandas.DataFrame(df["Omegadot"]).to_numpy().astype(np.float)
toc = pandas.DataFrame(df["tOC"]).to_numpy().astype(np.float)
af0 = pandas.DataFrame(df["af0"]).to_numpy().astype(np.float)
af1 = pandas.DataFrame(df["af1"]).to_numpy().astype(np.float)
af2 = pandas.DataFrame(df["af2"]).to_numpy().astype(np.float)


file_path2 = 'C:\\Users\\kpb18108\\Desktop\\IAA_GNSS\\Parsed_Data\\Parsed_raw_202001150949_rover2_Measurements.csv'
df2 = pandas.read_csv(file_path2)

tow = pandas.DataFrame(df2["TOW"]).to_numpy().astype(np.int64)        
week = pandas.DataFrame(df2["Week"]).to_numpy().astype(np.float) #-tau
leaps = pandas.DataFrame(df2["LeapS"]).to_numpy().astype(np.float)
svIdM = pandas.DataFrame(df2["svId"]).to_numpy().astype(np.int64)
pr_raw = pandas.DataFrame(df2["prMes"]).to_numpy().astype(np.float)
Numsv = pandas.DataFrame(df2["numMeas"]).to_numpy().astype(np.float)

# initial clock bias
b = 0
pr = []
# 		% Signal transmission time
tau = 0 # for now 0
# Speed of light
c = 299792458
# Earth's rotation rate
omega_e = 7.2921151467e-5
#initial position of the user
xu = np.array([0, 0, 0])

user_position_arr = []
user_clock_bias_arr = []

class Object(object):
    pass

sv_arr = np.unique(svid)
tow_arr = np.unique(tow)
svM_arr = np.unique(svIdM)  
eph = Object()
x0 = [0,0,0]

for i in range(0,len(tow_arr)):
    
    Xs = []
    dsv_all = []
    pr_all = []
    # find indicies of all entries corresponding to this satellite
    # sv = sv_arr[i]
    # idx = np.where(svid == sv)
    idxt = np.where(np.abs(tow-tow_arr[i]) == 0)
    # idx_sv = np.where(svid[] == svIdM[idx_tow[:][0]])
    # idx = idx_tow[0][idx_sv[:][0]]
    
    # idxM = np.where(svIdM[i] == sv)
    meastime = tow[idxt]
    pr_rawM = pr_raw[idxt]
    sv_idM = svIdM[idxt]
    numMeas = len(idxt)
    
    for j in range(0,numMeas):
        
        rcvr_tow = meastime[j]
        pr = pr_rawM[j]
        
        # Find EPHIMERIS data closest in time with same SVID
        
        #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!   
        
        eph_idx = np.where(np.abs(toc-meastime[j]) == np.nanmin(np.abs(toc-meastime[j])))
        sv_idx = np.where(svid[eph_idx] == sv_idM[j])
        idx = eph_idx[0][sv_idx]
    
        eph.t = t[idx,0]
        eph.sqrtA = sqrtA[idx,0]
        eph.toe = toe[idx,0]
        eph.dn = dn[idx,0]
        eph.m0 = m0[idx,0]
        eph.e = e[idx,0]
        eph.w = w[idx,0]
        eph.cus = cus[idx,0]
        eph.cuc = cuc[idx,0]
        eph.crs = crs[idx,0]
        eph.crc = crc[idx,0]
        eph.cis = cis[idx,0]
        eph.cic = cic[idx,0]
        eph.i0 = i0[idx,0]
        eph.idot = idot[idx,0]
        eph.omg0 = omg0[idx,0]
        eph.odot = odot[idx,0]
        eph.toc = toc[idx,0]
        eph.af0 = af0[idx,0]
        eph.af1 = af1[idx,0]
        eph.af2 = af2[idx,0]
    
        for k in range(0, len(idx)):  
            
            ep = Object()
            
            # eph_idx = np.where(np.abs(eph.toc-rcvr_tow) == np.nanmin(np.abs(eph.toc-rcvr_tow)))            
            
            ep.t = eph.t[k]
            ep.sqrtA = eph.sqrtA[k]
            ep.toe = eph.toe[k]
            ep.dn = eph.dn[k]
            ep.m0 = eph.m0[k]
            ep.e = eph.e[k]
            ep.w = eph.w[k]
            ep.cus = eph.cus[k]
            ep.cuc = eph.cuc[k]
            ep.crs = eph.crs[k]
            ep.crc = eph.crc[k]
            ep.cis = eph.cis[k]
            ep.cic = eph.cic[k]
            ep.i0 = eph.i0[k]
            ep.idot = eph.idot[k]
            ep.omg0 = eph.omg0[k]
            ep.odot = eph.odot[k]
            ep.toc = eph.toc[k]
            ep.af0 = eph.af0[k]
            ep.af1 = eph.af1[k]
            ep.af2 = eph.af2[k]
            
            dsv = estimate_satellite_clock_bias(rcvr_tow, ep)
        
             # Get satellite position
            [xs ,ys, zs] = get_satellite_positionh(ep, rcvr_tow, 1)  
            
            Xs.append([xs,ys,zs])
            dsv_all.append(dsv)
            pr_all.append(pr + c*dsv)
    
        # bla, _ = raw_gnss.calc_pos_fix(Xs, x0=[0, 0, 0, 0, 0], no_weight=False, signal='C1C')
    
    Fx_pos = estimate_position(Xs, pr_all, x0, dsv_all) 
    opt_pos = opt.least_squares(Fx_pos, x0).x     
    
    xu = opt_pos
    [lam, phi, h] = WGStoEllipsoid(xu[0], xu[1], xu[2])
    
    lat = phi*180./np.pi
    lon = lam*180./np.pi
    
    print(lat, lon)
        
        
        
        
        
        