# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 17:04:37 2020

@author: kpb18108
"""

from datetime import datetime, timedelta            
from tqdm import tqdm
import os
import csv
import numpy as np
import pandas
from sympy import *

class POSITION(): 
    def __init__(self, file_path: object):
        print('Hello')
        df = pandas.read_csv(file_path)
        
        # 		% Signal transmission time
        tau = 0 # for now 0
        
        svid = pandas.DataFrame(df["svId2"]).to_numpy().astype(np.int64)        
        t = pandas.DataFrame(df["TOW"]).to_numpy().astype(np.float) #-tau
        sqrtA = pandas.DataFrame(df["almsqrtA"]).to_numpy().astype(np.float)
        toe = pandas.DataFrame(df["tOE2"]).to_numpy().astype(np.float)
        dn = pandas.DataFrame(df["Deltan2"]).to_numpy().astype(np.float)
        m0 = pandas.DataFrame(df["M02"]).to_numpy().astype(np.float)
        e = pandas.DataFrame(df["alme"]).to_numpy().astype(np.float)
        w = pandas.DataFrame(df["almomega"]).to_numpy().astype(np.float)
        cus = pandas.DataFrame(df["Cus2"]).to_numpy().astype(np.float)
        cuc = pandas.DataFrame(df["Cuc2"]).to_numpy().astype(np.float)
        crs = pandas.DataFrame(df["Crs2"]).to_numpy().astype(np.float)
        crc = pandas.DataFrame(df["Crc3"]).to_numpy().astype(np.float)
        cis = pandas.DataFrame(df["Cis3"]).to_numpy().astype(np.float)
        cic = pandas.DataFrame(df["Cic3"]).to_numpy().astype(np.float)
        i0 = pandas.DataFrame(df["i03"]).to_numpy().astype(np.float)
        idot = pandas.DataFrame(df["IDOT3"]).to_numpy().astype(np.float)
        omg0 = pandas.DataFrame(df["Omega03"]).to_numpy().astype(np.float)
        odot = pandas.DataFrame(df["Omegadot3"]).to_numpy().astype(np.float)
        
        class Object(object):
            pass
        
        sv_arr = np.unique(svid)  
        eph = Object()
        
        for i in range(0,len(sv_arr)):
            # find indicies of all entries corresponding to this satellite
            sv = sv_arr[i];
            idx = np.where(svid == sv)
            eph.t = t[idx,0]
            eph.sqrtA = sqrtA[idx,0]
            eph.toe = toe[idx,0]
            eph.dn = dn[idx,0]
            eph.m0 = m0[idx,0]
            eph.e = e[idx,0]
            eph.w = w[idx,0]
            eph.cus = cus[idx,0]
            eph.cuc = cuc[idx,0]
            eph.crs = crs[idx,0]
            eph.crc = crc[idx,0]
            eph.cis = cis[idx,0]
            eph.cic = cic[idx,0]
            eph.i0 = i0[idx,0]
            eph.idot = idot[idx,0]
            eph.omg0 = omg0[idx,0]
            eph.odot = odot[idx,0]
            
        
        
        for i in range(0,len(sv_arr)):
            [x,y,z] = self.get_satellite_position(sqrtA[i,0], toe[i,0], t[i,0], 
                            dn[i,0], m0[i,0], e[i,0], w[i,0], cus[i,0], cuc[i,0], 
                            crs[i,0], crc[i,0], cis[i,0], cic[i,0], i0[i,0], 
                            idot[i,0], omg0[i,0], odot[i,0])
        
        print(x,y,z)
        
    def get_satellite_position(self, sqrtA, toe, t, dn, m0, e, w, cus, cuc, 
                               crs, crc, cis, cic, i0, idot, omg0, odot):
        """computes the position of a satellite at time (t) given the
     	% ephemeris parameters. 
     	% Usage: [x y z] =  get_satellite_position(eph, t, compute_harmonic_correction)
     	% Input Args: eph: ephemeris data
     	%             t: time 
     	%             compute_harmonic_correction (optional): 1 if harmonic
     	%             correction should be applied, 0 if not. 
     	% Output Args: [x y z] in ECEF in meters
     	% ephmeris data must have the following fields:
     	% rcvr_tow (receiver tow)
     	% svid (satellite id)
     	% toc (reference time of clock parameters)
     	% toe (referece time of ephemeris parameters)
     	% af0, af1, af2: clock correction coefficients
     	% ura (user range accuracy)
     	% e (eccentricity)
     	% sqrtA (sqrt of semi-major axis)
     	% dn (mean motion correction)
     	% m0 (mean anomaly at reference time)
     	% w (argument of perigee)
     	% omg0 (lontitude of ascending node)
     	% i0 (inclination angle at reference time)
     	% odot (rate of right ascension)
     	% idot (rate of inclination angle)
     	% cus (argument of latitude correction, sine)
     	% cuc (argument of latitude correction, cosine)
     	% cis (inclination correction, sine)
     	% cic (inclination correction, cosine)
     	% crs (radius correction, sine)
     	% crc (radius correction, cosine)
     	% iod (issue of data number) """
 
 
        # set default value for harmonic correction
        # switch nargin
        # case 2
        compute_harmonic_correction=1
        # 
        mu = 3.986005e14;
        omega_dot_earth = 7.2921151467e-5 #(rad/sec)
         
        # Now follow table 20-IV
        A = sqrtA**2
        cmm = np.sqrt(mu/A**3) # computed mean motion
        tk = t - toe
        # account for beginning of end of week crossover
        if (tk > 302400):
            tk = tk-604800
        
        if (tk < -302400):
            tk = tk+604800
         
        # apply mean motion correction
        n = cmm + dn
         
        # Mean anomaly
        mk = m0 + n*tk
         
        # solve for eccentric anomaly
        E = symbols('E')
        eqn = Eq(E - e*sin(E) == mk, E)
        sol = solve(eqn)
        Ek = float(sol)
         
        # True anomaly:
        nu = np.atan2((np.sqrt(1-e**2))*np.sin(Ek)/(1-e*np.cos(Ek)), 
                      (np.cos(Ek)-e)/(1-e*np.cos(Ek)))
        Ek = np.acos((e  + np.cos(nu))/(1+e*np.cos(nu)))
         
        Phi = nu + w
        du = 0
        dr = 0
        di = 0
        if (compute_harmonic_correction == 1):            
            # compute harmonic corrections
            du = cus*np.sin(2*Phi) + cuc*np.cos(2*Phi)
            dr = crs*np.sin(2*Phi) + crc*np.cos(2*Phi)
            di = cis*np.sin(2*Phi) + cic*np.cos(2*Phi)
        
         
        u = Phi + du
        r = A*(1-e*np.cos(Ek)) + dr
         
        # inclination angle at reference time
        i = i0 + idot*tk + di
        x_prime = r*np.cos(u)
        y_prime = r*np.sin(u)
        omega = omg0 + (odot - omega_dot_earth)*tk - omega_dot_earth*toe
         
        x = x_prime*np.cos(omega) - y_prime*np.cos(i)*np.sin(omega)
        y = x_prime*np.sin(omega) + y_prime*np.cos(i)*np.cos(omega)
        z = y_prime*np.sin(i)
        
        return(x,y,z)