import psycopg2 as ps


def get_db_connection():
    host = '130.159.16.176'
    dbname = 'wind_farm_analytics'
    user = 'postgres'
    password = 'postgres'
    port = '5432'

    conn_string = "dbname=%s port=%s user=%s password=%s host=%s" % (dbname, port, user, password, host)
    return ps.connect(conn_string)