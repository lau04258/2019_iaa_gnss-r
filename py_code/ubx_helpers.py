import ray
import pandas as pd
from datetime import datetime, timedelta
from time import time
from psycopg2.extras import execute_values
from config import get_db_connection
from exceptions import database_commit_exception as dce

def rcv_tow_2_utc(rcvtow: int, week: int, leap_seconds: int) -> datetime.time:
    """
    Convert GPS Time Of Week to UTC time.

    :param int itow: GPS Time Of Week
    :return: UTC time hh.mm.ss
    :rtype: datetime.time

    """

    utc = datetime(1980, 1, 6) + timedelta(weeks=week, seconds=rcvtow - leap_seconds)

    return utc


# @ray.remote
def get_parsed_payloads(message_list, dataframe=True, db_table_name=None):
    parsed_payloads = list()

    for message in message_list:
        try:  # this is a check to make sure that if we have a bad payload we can move past it.
            message.get_UBX_message()
            if message.grouped_attribs:  # Check to see if we have a complete payload, if not bin payload
                if not dataframe:
                    parsed_payloads.append(message)
                else:
                    parsed_payloads.append(message.get_dataframe())
        except:
            print('bad payload, skipping to the next one')
            pass


    if parsed_payloads and dataframe:
        concat_df = get_concatenated_dataframes(parsed_payloads)
        try:
            post_to_database(concat_df, db_table_name)
        except:
            pass
    elif parsed_payloads:
        return parsed_payloads


def get_flat_list(list_of_lists):
    return [val for sublist in list_of_lists for val in sublist]


def get_chunks(lst: list, n):
    """List of successive n-sized chunks from lst."""
    list_chunks = []
    for x in range(0, len(lst), n):
        list_chunks.append(lst[x:x + n])
    return list_chunks


def get_concatenated_dataframes(dataframe_list: list, name=None):
    full_dataframe = pd.concat(dataframe_list)
    if name:
        full_dataframe.name = name
    return full_dataframe


def post_to_database(data_frame, db_table_name):
    """
    This method posts dataframes to a remote server running PostgreSQL database. Connection is specified in the config file
    :return:
    """
    # method to post data to a database that can be used for analysis at a later date.
    connection = get_db_connection()
    connection.set_session(autocommit=True)
    tuples = [tuple(x) for x in data_frame.to_numpy()]
    cols = ','.join(list(data_frame.columns))
    query = "INSERT INTO %s(%s) VALUES %%s " % (db_table_name, cols)
    with connection.cursor() as cursor:
        execute_values(cursor, query, tuples)

@ray.remote
def parse_LLH_payload(dir_path, chunk):
    for file in chunk:
        if file.endswith(".LLH"):
            print('Processing', file)
            try:
                file_path = dir_path + '/' + file
                data = get_LLH_data(file_path)
                data = data.dropna()   #  drop nan rows
                db_table_name = dir_path.split('\\')[-1] + '_llh'
                post_to_database(data, db_table_name)
                print('Processed', file)
            except:
                print(file, 'failed')

def get_LLH_data(filename):
    data = pd.read_csv(filename, sep='\s{2,}',
                       names=['utc_time', 'lat', 'lon', 'alt', 'sig_lock', 'no_sats', 'sdn',
                              'sde', 'sdu', 'sdne', 'sdeu', 'sdun', 'age', 'ratio'], on_bad_lines='warn')

    return data