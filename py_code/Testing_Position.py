# -*- coding: utf-8 -*-
"""
Created on Mon Oct 12 15:15:57 2020

@author: kpb18108
"""

from datetime import datetime, timedelta            
from tqdm import tqdm
import os
import csv
import numpy as np
import pandas
from sympy import Symbol, nsolve, sin, cos
from math import fabs, atan2
import scipy.optimize as opt



def estimate_satellite_clock_bias(t, eph):
    F = -4.442807633e-10
    mu = 3.986005e14
    A = eph.sqrtA**2
    cmm = np.sqrt(mu/A**3)# % computed mean motion
    tk = t - eph.toe
    #account for beginning or end of week crossover
    if tk > 302400:
        tk = tk-604800
    
    if tk < -302400:
        tk = tk+604800
	
	# apply mean motion correction
    n = cmm + eph.dn
 
	# Mean anomaly
    mk = eph.m0 + n*tk
    
    # solve for eccentric anomaly
    E = Symbol('E',real = True) 
    Ek = nsolve(E - eph.e*sin(E) - mk, 0)    
    
    dsv = eph.af0 + eph.af1*(t-eph.toc) + eph.af2*(t-eph.toc)**2 + F*eph.e*eph.sqrtA*np.sin(float(Ek))
    
    return(dsv)

def WGStoEllipsoid(x,y,z):
    # WGStoEllipsoid Convert ECEF coordinates to Ellipsoidal (longitude, latitude, height above ellipsoid)
	# Usage: [lambda, phi, h] =  WGStoEllipsoid(x,y,z)
	# Input Args: coordinates in ECEF
	# Output Args: Longitude, Latitude in radians, height in meters
    
    x=np.array(x,dtype=float)
    y=np.array(y,dtype=float)
    z=np.array(z,dtype=float)
    
	# WGS ellipsoid params
    a = 6378137
    f = 1/298.257
    e = np.sqrt(2*f-f**2)
    # From equation 4.A.3,
    lam = atan2(y,x)
    p = np.sqrt(x**2+y**2)
 
    # initial value of phi assuming h = 0;
    h = 0
    phi = np.arctan2(z,p*(1-e**2)) #4.A.5
    N = a/(1-(e*np.sin(phi))**2)**0.5    
    delta_h = 1000000
    while delta_h > 0.01:
        prev_h = h
        phi = atan2(z,p*(1-e**2*(N/(N+h)))) #4.A.5
        N = a/(1-(e*np.sin(phi))**2)**0.5
        h = p/np.cos(phi)-N
        delta_h = abs(h-prev_h)
        
    return(lam, phi, h)

def estimate_position(xs, pr, numSat, x0, b0, dim):
    # estimate_position: estimate the user's position and user clock bias
    # Usage: [x, b, norm_dp, G] = estimate_position(xs, pr, numSat, x0, b0, dim)
    # Input Args: xs: satellite position matrix
    #             pr: corrected pseudo ranges (adjusted for known value of the
    #             satellite clock bias)
    #             numSat: number of satellites
    #             x0: starting estimate of the user position
    #             b0: starting point for the user clock bias
    #             dim: dimensions of the satellite vector. 3 for 3D, 2 for 2D
    # Notes: b and b0 are usually 0 as the current estimate of the clock bias
    # has already been applied to the input pseudo ranges.
    # Output Args: x: optimized user position
    #              b: optimized user clock bias
    #              norm_dp: normalized pseudo-range difference
    #              G: user satellite geometry matrix, useful for computing DOPs
    
    # dx = 100*(np.zeros((1, dim))+1)
    # db = 0
    # norm_dp = 100
    # numIter = 0
    
    # while np.linalg.norm(dx) > 1e-3:
    #     norms = np.sqrt(np.sum((xs-x0)**2,1,dtype=float))
    #     # delta pseudo range:
    #     dp = pr - norms + b - b0
    #     G = np.append(-(xs-x0)/norms[:,None], np.ones((numSV,1)),1)
    #     G = np.array(G,dtype=float)
    #     GT = np.matrix.transpose(G)
    #     sol = np.linalg.inv(GT*G)*GT*dp
    #     dx = np.matrix.transpose(sol[0,0:dim])
    #     db = sol[0,dim]
    #     dp_float = np.array(dp, dtype=float)
    #     norm_dp = np.linalg.norm(dp_float)
    #     numIter = numIter + 1
    #     x0 = x0 + dx
    #     b0 = b0 + db
    #     dx = np.array(dx,dtype=float)
    
    def Fx_pos(pos):
        rows = []
                     
        EARTH_ROTATION_RATE = 7.2921151467e-5
        SPEED_OF_LIGHT = 299792458      
        weight = 1
        bc = 0      
        sat_pos = xs
        theta = EARTH_ROTATION_RATE * (pr - bc) / SPEED_OF_LIGHT
        rows.append(weight * (np.sqrt(
              (sat_pos[0] * np.cos(theta) + sat_pos[1] * np.sin(theta) - pos[0])**2 +
              (sat_pos[1] * np.cos(theta) - sat_pos[0] * np.sin(theta) - pos[1])**2 +
              (sat_pos[2] - pos[2])**2) - (pr - bc)))
          
        return rows
  
    opt_pos = opt.least_squares(Fx_pos, x0)
    
    
    return(opt_pos)
    #return(x, b, norm_dp, G)






def get_satellite_positionh(eph, t, compute_harmonic_correction):
    """computes the position of a satellite at time (t) given the
 	% ephemeris parameters. 
 	% Usage: [x y z] =  get_satellite_position(eph, t, compute_harmonic_correction)
 	% Input Args: eph: ephemeris data
 	%             t: time 
 	%             compute_harmonic_correction (optional): 1 if harmonic
 	%             correction should be applied, 0 if not. 
 	% Output Args: [x y z] in ECEF in meters
 	% ephmeris data must have the following fields:
 	% rcvr_tow (receiver tow)
 	% svid (satellite id)
 	% toc (reference time of clock parameters)
 	% toe (referece time of ephemeris parameters)
 	% af0, af1, af2: clock correction coefficients
 	% ura (user range accuracy)
 	% e (eccentricity)
 	% sqrtA (sqrt of semi-major axis)
 	% dn (mean motion correction)
 	% m0 (mean anomaly at reference time)
 	% w (argument of perigee)
 	% omg0 (lontitude of ascending node)
 	% i0 (inclination angle at reference time)
 	% odot (rate of right ascension)
 	% idot (rate of inclination angle)
 	% cus (argument of latitude correction, sine)
 	% cuc (argument of latitude correction, cosine)
 	% cis (inclination correction, sine)
 	% cic (inclination correction, cosine)
 	% crs (radius correction, sine)
 	% crc (radius correction, cosine)
 	% iod (issue of data number) """
 
 
    # set default value for harmonic correction
    # switch nargin
    # case 2
    compute_harmonic_correction=1
    # 
    mu = 3.986005e14 # WGS 84 value of the earth's gravitational constant for GPS user
    omega_dot_earth = 7.2921151467e-5 #(rad/sec) WGS84 value of the Earth's rotation rate
     
    # Now follow table 20-IV
    A = eph.sqrtA*eph.sqrtA
    cmm = np.sqrt(mu/A**3) # computed mean motion n_0
    tk = t - eph.toe
    # account for beginning of end of week crossover
    if (tk > 302400):
        tk = tk-604800
    
    if (tk < -302400):
        tk = tk+604800
     
    # apply mean motion correction
    n = cmm + eph.dn
     
    # Mean anomaly
    mk = eph.m0 + n*tk
     
    # solve for eccentric anomaly
    E = Symbol('E')
    Ek = float(nsolve(E - eph.e*sin(E), mk))

     
    # True anomaly:
    # arc1 = (np.sqrt(1-eph.e**2))*np.sin(Ek) #/(1-eph.e*np.cos(Ek))
    # arc2 = (np.cos(Ek)-eph.e) #/(1-eph.e*np.cos(Ek))
    
    # temp2d = np.sqrt(1.0 - eph.e * eph.e)
    
    nu = np.arctan(((np.sqrt(1.-eph.e**2)*np.sin(Ek))/(1.-eph.e*np.cos(Ek))) / 
                 ((np.cos(Ek)-eph.e)/(1.-eph.e*np.cos(Ek))))
    
    # nu = np.arctan2(temp2d * sin(Ek), cos(Ek) - eph.e)
    # nu = np.arctan2(arc1,arc2)
    Ek = np.arccos((eph.e  + np.cos(nu))/(1+eph.e*np.cos(nu)))
     
    Phi = nu + eph.w
    du = 0
    dr = 0
    di = 0
    
    # Second Harmon Perturbations
    du = eph.cus*np.sin(2*Phi) + eph.cuc*np.cos(2*Phi)
    dr = eph.crs*np.sin(2*Phi) + eph.crc*np.cos(2*Phi)
    di = eph.cis*np.sin(2*Phi) + eph.cic*np.cos(2*Phi)
    
    # Corrected Argument of Latitude 
    u = Phi + du
    # Corrected Radius
    r = A*(1-eph.e*np.cos(Ek)) + dr     
    # inclination angle at reference time
    i = float(eph.i0 + eph.idot*tk + di)
    
    # Positions in Orbital Plane:
    x_prime = float(r*np.cos(u))
    y_prime = float(r*np.sin(u))
    
    # Corrected Longitude of Ascending Node
    omega = float(eph.omg0 + (eph.odot - omega_dot_earth)*tk - omega_dot_earth*eph.toe)
    
    # Earth fixed coordinates
    x = x_prime*np.cos(omega) - y_prime*np.cos(i)*np.sin(omega)
    y = x_prime*np.sin(omega) + y_prime*np.cos(i)*np.cos(omega)
    z = y_prime*np.sin(i)
    
    return(x,y,z)


def get_sat_info(eph, t, compute_harmonic_correction):
    
    EARTH_GM = 3.986005e14
    EARTH_ROTATION_RATE = 7.2921151467e-5
    tdiff = t - eph.toc  # Time of clock
    # clock_err = eph.af0 + tdiff * (eph.af1 + tdiff * eph.af2)
    # clock_rate_err = eph.af1 + 2 * tdiff * eph.af0

    # Orbit propagation
    tdiff = t - eph.toe  # Time of ephemeris (might be different from time of clock)

    # Calculate position per IS-GPS-200D p 97 Table 20-IV
    a = eph.sqrtA * eph.sqrtA  # [m] Semi-major axis
    ma_dot = np.sqrt(EARTH_GM / (a * a * a)) + eph.dn  # [rad/sec] Corrected mean motion
    ma = eph.m0 + ma_dot * tdiff  # [rad] Corrected mean anomaly

    # Iteratively solve for the Eccentric Anomaly (from Keith Alter and David Johnston)
    ea = ma  # Starting value for E

    ea_old = 2222
    while fabs(ea - ea_old) > 1.0E-14:
      ea_old = float(ea)
      tempd1 = 1.0 - eph.e * np.cos(ea_old)
      ea = ea + (ma - ea_old + eph.e * np.sin(ea_old)) / tempd1
    # ea_dot = float(ma_dot / tempd1)
    
    ea = float(ea)
    # Relativistic correction term
    # einstein = -4.442807633E-10 * eph.e * eph.sqrtA * np.sin(ea)

    # Begin calc for True Anomaly and Argument of Latitude
    tempd2 = np.sqrt(1.0 - eph.e * eph.e)
    # [rad] Argument of Latitude = True Anomaly + Argument of Perigee
    al = atan2(tempd2 * np.sin(ea), np.cos(ea) - eph.e) + eph.w
    # al_dot = tempd2 * ea_dot / tempd1

    # Calculate corrected argument of latitude based on position
    cal = al + eph.cus * np.sin(2.0 * al) + eph.cus * np.cos(2.0 * al)
    # cal_dot = al_dot * (1.0 + 2.0 * (eph.cus * np.cos(2.0 * al) -
    #                                  eph.cuc * np.sin(2.0 * al)))

    # Calculate corrected radius based on argument of latitude
    r = a * tempd1 + eph.crc * np.cos(2.0 * al) + eph.crs * np.sin(2.0 * al)
    # r_dot = (a * eph.e * np.sin(ea) * ea_dot +
    #          2.0 * al_dot * (eph.crs * np.cos(2.0 * al) -
    #                          eph.crc * np.sin(2.0 * al)))

    # Calculate inclination based on argument of latitude
    inc = (eph.i0 + eph.idot * tdiff +
           eph.cic * np.cos(2.0 * al) +
           eph.cis * np.sin(2.0 * al))
    # inc_dot = (eph.idot +
    #            2.0 * al_dot * (eph.cis * np.cos(2.0 * al) -
    #                            eph.cic * np.sin(2.0 * al)))

    # Calculate position and velocity in orbital plane
    x = r * np.cos(cal)
    y = r * np.sin(cal)
    # x_dot = r_dot * np.cos(cal) - y * cal_dot
    # y_dot = r_dot * np.sin(cal) + x * cal_dot

    # Corrected longitude of ascending node
    om_dot = eph.odot - EARTH_ROTATION_RATE
    om = eph.omg0 + tdiff * om_dot - EARTH_ROTATION_RATE * eph.toe

    om = float(om)
    inc = float(inc)
    # Compute the satellite's position in Earth-Centered Earth-Fixed coordiates
    pos = np.empty(3)
    pos[0] = x * np.cos(om) - y * np.cos(inc) * np.sin(om)
    pos[1] = x * np.sin(om) + y * np.cos(inc) * np.cos(om)
    pos[2] = y * np.sin(inc)
    
    return(pos[0],pos[1],pos[2])



file_path = 'C:\\Users\\kpb18108\\Desktop\\IAA_GNSS\\Parsed_Data\\Parsed_raw_202001150949_rover2_BroadcastMessage_New.csv'
df = pandas.read_csv(file_path)

svid = pandas.DataFrame(df["svId2"]).to_numpy().astype(np.int64)        
t = pandas.DataFrame(df["WN"]).to_numpy().astype(np.float) #-tau
sqrtA = pandas.DataFrame(df["sqrtA"]).to_numpy().astype(np.float)
toe = pandas.DataFrame(df["tOE"]).to_numpy().astype(np.float)
dn = pandas.DataFrame(df["Deltan"]).to_numpy().astype(np.float)
m0 = pandas.DataFrame(df["M0"]).to_numpy().astype(np.float)
e = pandas.DataFrame(df["e"]).to_numpy().astype(np.float)
w = pandas.DataFrame(df["omega"]).to_numpy().astype(np.float)
cus = pandas.DataFrame(df["Cus"]).to_numpy().astype(np.float)
cuc = pandas.DataFrame(df["Cuc"]).to_numpy().astype(np.float)
crs = pandas.DataFrame(df["Crs"]).to_numpy().astype(np.float)
crc = pandas.DataFrame(df["Crc"]).to_numpy().astype(np.float)
cis = pandas.DataFrame(df["Cis"]).to_numpy().astype(np.float)
cic = pandas.DataFrame(df["Cic"]).to_numpy().astype(np.float)
i0 = pandas.DataFrame(df["i0"]).to_numpy().astype(np.float)
idot = pandas.DataFrame(df["IDOT"]).to_numpy().astype(np.float)
omg0 = pandas.DataFrame(df["Omega0"]).to_numpy().astype(np.float)
odot = pandas.DataFrame(df["Omegadot"]).to_numpy().astype(np.float)
toc = pandas.DataFrame(df["tOC"]).to_numpy().astype(np.float)
af0 = pandas.DataFrame(df["af0"]).to_numpy().astype(np.float)
af1 = pandas.DataFrame(df["af1"]).to_numpy().astype(np.float)
af2 = pandas.DataFrame(df["af2"]).to_numpy().astype(np.float)

file_path2 = 'C:\\Users\\kpb18108\\Desktop\\IAA_GNSS\\Parsed_Data\\Parsed_raw_202001150949_rover2_Measurements.csv'
df2 = pandas.read_csv(file_path2)

tow = pandas.DataFrame(df2["TOW"]).to_numpy().astype(np.int64)        
week = pandas.DataFrame(df2["Week"]).to_numpy().astype(np.float) #-tau
leaps = pandas.DataFrame(df2["LeapS"]).to_numpy().astype(np.float)
svIdM = pandas.DataFrame(df2["svId"]).to_numpy().astype(np.int64)
pr_raw = pandas.DataFrame(df2["prMes"]).to_numpy().astype(np.float)
Numsv = pandas.DataFrame(df2["numMeas"]).to_numpy().astype(np.float)

# initial clock bias
b = 0
pr = []
# 		% Signal transmission time
tau = 0 # for now 0
# Speed of light
c = 299792458
# Earth's rotation rate
omega_e = 7.2921151467e-5
#initial position of the user
xu = np.array([0, 0, 0])

user_position_arr = []
user_clock_bias_arr = []



class Object(object):
    pass

sv_arr = np.unique(svid)
svM_arr = np.unique(svIdM)  
eph = Object()

for i in range(0,len(sv_arr)):
    # find indicies of all entries corresponding to this satellite
    sv = sv_arr[i];
    idx = np.where(svid == sv)
    
    eph.t = t[idx[0],0]
    eph.sqrtA = sqrtA[idx[0],0]
    eph.toe = toe[idx[0],0]
    eph.dn = dn[idx[0],0]
    eph.m0 = m0[idx[0],0]
    eph.e = e[idx[0],0]
    eph.w = w[idx[0],0]
    eph.cus = cus[idx[0],0]
    eph.cuc = cuc[idx[0],0]
    eph.crs = crs[idx[0],0]
    eph.crc = crc[idx[0],0]
    eph.cis = cis[idx[0],0]
    eph.cic = cic[idx[0],0]
    eph.i0 = i0[idx[0],0]
    eph.idot = idot[idx[0],0]
    eph.omg0 = omg0[idx[0],0]
    eph.odot = odot[idx[0],0]
    eph.toc = toc[idx[0],0]
    eph.af0 = af0[idx[0],0]
    eph.af1 = af1[idx[0],0]
    eph.af2 = af2[idx[0],0]
    
    idxM = np.where(svIdM == sv)
    meastime = tow[idxM]
    pr_rawM = pr_raw[idxM]
    numsvM = Numsv[idxM]
    
for idx in range(5, len(meastime), 5):  
    rcvr_tow = meastime[idx]
    # data block corresponding to this satellite
    nav_data = pr_rawM[5*(idx): 5*(idx+1)]
    # find indicies of rows containing non-zero data. Each row corresponds
    # to a satellite
    ind = np.where(~np.isnan(nav_data))
    numSV = len(ind[0])
    eph_formatted_ = [] 

    if numSV > 4:
        pr_ = []         
    
        for k in range(0,numSV):
            sv_idx = ind[0][k]
            sv_data = nav_data[sv_idx]
            # find ephemeris data closest to this time of week
            ep = Object()
            # Find EPHIMERIS data closest in time
            
            #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!        
            
            eph_idx = np.where(np.abs(eph.toc-rcvr_tow) == np.nanmin(np.abs(eph.toc-rcvr_tow)))            
            
            ep.t = np.nanmean(eph.t[eph_idx[0]])
            ep.sqrtA = np.nanmean(eph.sqrtA[eph_idx[0]+1])
            ep.toe = np.nanmean(eph.toe[eph_idx[0]+1])
            ep.dn = np.nanmean(eph.dn[eph_idx[0]+1])
            ep.m0 = np.nanmean(eph.m0[eph_idx[0]+1])
            ep.e = np.nanmean(eph.e[eph_idx[0]+1])
            ep.w = np.nanmean(eph.w[eph_idx[0]+2])
            ep.cus = np.nanmean(eph.cus[eph_idx[0]+1])
            ep.cuc = np.nanmean(eph.cuc[eph_idx[0]+1])
            ep.crs = np.nanmean(eph.crs[eph_idx[0]+1])
            ep.crc = np.nanmean(eph.crc[eph_idx[0]+2])
            ep.cis = np.nanmean(eph.cis[eph_idx[0]+2])
            ep.cic = np.nanmean(eph.cic[eph_idx[0]+2])
            ep.i0 = np.nanmean(eph.i0[eph_idx[0]+2])
            ep.idot = np.nanmean(eph.idot[eph_idx[0]+2])
            ep.omg0 = np.nanmean(eph.omg0[eph_idx[0]+2])
            ep.odot = np.nanmean(eph.odot[eph_idx[0]+2])
            ep.toc = np.nanmean(eph.toc[eph_idx[0]])
            ep.af0 = np.nanmean(eph.af0[eph_idx[0]])
            ep.af1 = np.nanmean(eph.af1[eph_idx[0]])
            ep.af2 = np.nanmean(eph.af2[eph_idx[0]])
            
            
            # To be correct, the satellite clock bias should be calculated
            # at rcvr_tow - tau, however it doesn't make much difference to
            # do it at rcvr_tow
            dsv = estimate_satellite_clock_bias(rcvr_tow, ep)
            # measured pseudoranges corrected for satellite clock bias.
            # Also apply ionospheric and tropospheric corrections if
            # available
            pr_raw = sv_data
            pr_.append(pr_raw + c*dsv)
            
        # Get satellite position
        [xs ,ys, zs] = get_satellite_positionh(ep, rcvr_tow, 1)       
        final = estimate_position([xs,ys,zs], pr_raw, numSV, xu, b, 3)
            
        xu = final.x
        print(xu)    
        # print(xu[0], xu[1], xu[2])
        [lam, phi, h] = WGStoEllipsoid(xu[0], xu[1], xu[2])
        
        lat = phi*180/np.pi
        lon = lam*180/np.pi
        
        
        # print(lat, lon)
        # R1=np.linalg.rot(90+lon, 3)
        # R2=np.linalg.rot(90-lat, 1)
        # R=R2*R1
        # G_ = [G[:,1:3]*np.linalg.transpose(R), G[:,4]]
        # H = np.linalg.inv(np.linalg.transpose(G_)*G_)
        # HDOP = np.sqrt(H[1,1] + H[2,2])
        # VDOP = np.sqrt(H[3,3])
        # # Record various quantities for saving and plotting
        # HDOP_arr = HDOP
        # VDOP_arr = VDOP
        user_position_arr.append([lat, lon, h])
        user_clock_bias_arr.append(b)
            
# print(user_position_arr)
