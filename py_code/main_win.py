
import CONSTANTS as const
from UBX_streaming import UBX_stream
import os
import ray
from time import time
from exceptions import UBX_parsing_exception as ube

def main_process():

    #processed directory path
    path_processed = 'C:/Users/mjb16198/Desktop/IAA_Project/Turbine_data/Levenmouth_test_data/processed'
    path_failed =  'C:/Users/mjb16198/Desktop/IAA_Project/Turbine_data/Levenmouth_test_data/failed'
    root_dir = 'C:/Users/mjb16198/Desktop/IAA_Project/Turbine_data/Levenmouth_test_data/unprocessed'

    t0 = time()

    for dir, sub_dir, files in os.walk(root_dir):
        for file in files:
            if file.endswith(".UBX"):
                db_table_name = dir.split('\\')[-2]
                print('Identifying payloads for', db_table_name)

                print ('Processing', file)
                try:
                    rover = UBX_stream(os.path.join(dir, file), dir, const.message_headers['UBX'],
                                   const.message_structure['UBX-RXM-RAWX']['class'],
                                   const.message_structure['UBX-RXM-RAWX']['id'],
                                   const.UBX_RXM_RAWX_payload, db_table_name
                                    )
                    print('Parsing ', len(rover.payloads), 'payloads')
                    rover.parse_payloads(dataframe=True, multiprocessing=True)
                    t1 = time()
                    print('It took ', (t1 - t0) / 60, 'mins to parse and commit all payloads\n')
                    # Move file to processed directory - note that the processed directory must exist
                    # os.replace(dir + '/' + file, path_processed + '/' + db_table_name + '/' + file)
                except ube:
                    print('File', file, ' processing failed, moving to failed folder for review')
                    # os.replace(root_dir + '/' + file, path_failed + '/' + db_table_name + '/' + file)
                    pass


if __name__ == '__main__':
    main_process()