import CONSTANTS as const
from UBX_streaming import UBX_stream
import psutil
import os
import pandas as pd
import ray
from time import time
from ubx_helpers import post_to_database, get_chunks, get_LLH_data, parse_LLH_payload


# Do no of messages based on processor with Ray
# Date, lat, lon, elevation, quality_flag, numebr of valid sats, sdn, sde, sdu, sdne, sdeu, sdun, age of differential, ratio factor
# # (lat/lon/height=WGS84/ellipsoidal,Q=1:fix,2:float,4:dgps,5:single,ns=# of satellites) % GPST latitude(deg) longitude(deg) height(m) Q ns sdn(m) sde(m) sdu(m) sdne(m) sdeu(m) sdun(m) age(s) ratio
# 2021/09/10 15:49:31.200   56.173581828   -3.019202329   164.0855   5  23   1.8779   1.5066   4.2205  -0.4766  -0.4409   1.3995   0.00    0.0
def main_process():

    #processed directory path
    path_processed = 'C:/Users/mjb16198/Desktop/IAA_Project/Turbine_data/Levenmouth_test_data/processed/LLH'
    path_failed =  'C:/Users/mjb16198/Desktop/IAA_Project/Turbine_data/Levenmouth_test_data/failed/LLH'
    root_dir = 'C:/Users/mjb16198/Desktop/IAA_Project/Turbine_data/Levenmouth_test_data/unprocessed/LLH'

    t0 = time()
    num_cpus = psutil.cpu_count(logical=True)  # get number of processors to split across
    ray.init(num_cpus=num_cpus)

    for dir, sub_dir, files in os.walk(root_dir):
        # split files per processor here
        if len(files) != 0:
            t0 = time
            chunks = get_chunks(files, num_cpus)  # splitting up the data into number of chunks per processor
            ray.get([parse_LLH_payload.remote(dir, chunk) for chunk in chunks])
            # [parse_LLH_payload(dir, chunk) for chunk in chunks]


if __name__ == '__main__':
    main_process()

