
import CONSTANTS as const
from UBX_streaming import UBX_stream
import os
from time import time
from exceptions import UBX_parsing_exception as ube

def main_process():

    #processed directory path

    path_processed = '/Users/huli/Documents/repos.nosync/2019_iaa_gnss-r/Datas/processed/'
    path_failed =  '/Users/huli/Documents/repos.nosync/2019_iaa_gnss-r/Datas/failed/'
    root_dir = '/Users/huli/Documents/repos.nosync/2019_iaa_gnss-r/Datas/unprocessed/'

    t0 = time()

    for dir, sub_dir, files in os.walk(root_dir):
        db_table_name = dir.split('/')[-1]
        print('Identifying payloads for', db_table_name)
        for file in files:
            if file.endswith(".UBX"):
                print ('Processing', file)
                try:
                    rover = UBX_stream(os.path.join(dir, file), dir, const.message_headers['UBX'],
                                   const.message_structure['UBX-RXM-SFRBX']['class'],
                                   const.message_structure['UBX-RXM-SFRBX']['id'],
                                   const.UBX_RXM_SFRBX_payload, db_table_name
                                    )
                    print('Parsing ', len(rover.payloads), 'payloads')
                    rover.parse_payloads(dataframe=True, multiprocessing=True)
                    t1 = time()
                    print('It took ', (t1 - t0) / 60, 'mins to parse and commit all payloads\n')
                    # Move file to processed directory - note that the processed directory must exist
                    os.replace(dir + '/' + file, path_processed + db_table_name + '/' + file)
                except ube:
                    print('File', file, ' processing failed, moving to failed folder for review')
                    os.replace(dir + '/' + file, path_failed + db_table_name + '/' + file)
                    pass


if __name__ == '__main__':
    main_process()