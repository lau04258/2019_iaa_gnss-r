import struct
import re
from exceptions import UBX_parsing_exception as ube
from UBX_message import UBX_message
from psycopg2.extras import execute_values
from CONSTANTS import UBX_MSGIDS
from config import get_db_connection
import ray
import psutil
import time
from ubx_helpers import get_parsed_payloads, get_flat_list, get_chunks, get_concatenated_dataframes

class UBX_stream():
    """class that can be instantiated to parse data from the UBLOX receiver. Data is parsed according to the protocol specification provided @
     https://www.u-blox.com/sites/default/files/products/documents/u-blox8-M8_ReceiverDescrProtSpec_(UBX-13003221)_Public.pdf#page=299&zoom=100,0,0 """

    def __init__(self, filename: str, sensor_name: str,
                 message_header: bytes, message_class: int, message_id: int, payload_structure: dict, db_table_name: str):

        # extract the data and the message indexes we are interested in.
        self.payload_class_set = set()
        self.payload_class_id_set = set()
        self.filename = filename
        self.payloads = list()
        self.sensor_id = sensor_name
        self.mess_header = message_header
        self.mess_class = message_class
        self.mess_id = message_id
        self.messages = list()
        self.payload_structure = payload_structure
        self.parsed_payloads = list()
        self.data_frame = None
        self.db_table_name = db_table_name
        if self.filename.endswith('.UBX'):
            self.df_name = sensor_name + self.filename[:-4] # define the dataframe name

        # Methods run on initiation
        self.get_payloads()

    def get_payloads(self):
        """
        Method to find and store all the payloads that are present in the raw data package that match our criteria.
        The method also stores and prints the data packages identified in the payload
        :return:
        """

        with open(self.filename, 'rb') as f:
            data = f.read()

        # finding ubx indexes, allowing for the removal of the header (+1)
        indexes = [m.start() + 2 for m in re.finditer(self.mess_header, data)]

        # exit code if there is no message
        if len(indexes) == 0:
            raise ube('There is no matching message in this payload')

        for idx in indexes:
            # Get the payloads we are interested in
            # 2 byte sync code, 1 byte class, 1 byte ID, 2 byte length (Section 32.2 UBX FRAME STRUCTURE in doc)
            payload_class, payload_id, payload_len = struct.unpack_from('BBH', data, idx)

            # Store all the payload classes and ids for ref
            self.payload_class_set.add(struct.pack('B', payload_class))
            self.payload_class_id_set.add(data[idx:idx + 2])

            # If we have a matching payload then we store that payload for parsing later
            if self.mess_class == payload_class and self.mess_id == payload_id:
                payload = data[idx + 4: idx + 4 + payload_len]
                self.payloads.append(payload)
                self.messages.append(UBX_message(self.mess_header, self.mess_class, self.mess_id,
                                                 self.payload_structure, payload))
        if self.payloads is None:
            raise ube('There is no matching message in this payload')
        self.print_message_types()

    def parse_payloads(self, dataframe=True, multiprocessing=True):
        """
        Processing the payloads using the message class, and accounting for multiprocessing using the Ray package
        :param dataframe: boolean - flag to state whether we are outputting a dataframe or list (Default - true)
        :param multiprocessing: boolean - flag to state whether we are doing multiprocessing (Default - true)
        :return:
        """

        if multiprocessing:
            num_cpus = psutil.cpu_count(logical=True)  # get number of processors to split across
            # ray.init(num_cpus=num_cpus)
            chunks = get_chunks(self.messages, num_cpus) # splitting up the data into number of chunks per processor
            t_ray = time.time()
            # ray.get([get_parsed_payloads.remote(chunk, dataframe, self.db_table_name) for chunk in chunks])
            test = [get_parsed_payloads(chunk, dataframe, self.db_table_name) for chunk in chunks]
            print('ray took ', (time.time() - t_ray) / 60, 'mins')
            ray.shutdown()
        else:  # Note this could be very slow good.
            self.parsed_payloads = get_parsed_payloads(self.messages)

        # if we want a dataframe then we'll concatenate the list of dataframes from previous method
        # if dataframe:
        #     self.data_frame = get_concatenated_dataframes(self.parsed_payloads, self.df_name)

    def print_message_types(self):
        """
        This method prints out all the UBX messages that are present in this data package

        :return:
        """
        # print('This data packet contains the following UBX classes')
        # for ubx_class in self.payload_class_set:
        #     if ubx_class in UBX_CLASSES:
        #         print(UBX_CLASSES.get(ubx_class))
        print('This data packet contains the following UBX messages')
        for ubx_mess in self.payload_class_id_set:
            if ubx_mess in UBX_MSGIDS:
                print(UBX_MSGIDS.get(ubx_mess))






