import struct
from ubx_helpers import rcv_tow_2_utc
from collections import defaultdict
import pandas as pd
from exceptions import UBX_parsing_exception as ube

class UBX_message():

    def __init__(self, header, msg_class, msg_id, payload_structure, payload):
        self.grouped_attribs = defaultdict(list)  # default dict of lists to store a measurement block > 1
        self.single_attribs = dict()
        self._header = header
        self._class = msg_class
        self._msg_id = msg_id
        self.message_struct = payload_structure
        self.payload = payload
        self.utc_time = None
        self.parsed_flag = False

    def get_UBX_message(self):
        self.parsed_flag = True
        offset = 0
        for key in self.message_struct.keys():
            if isinstance(self.message_struct[key], tuple): # check for repeated block
                # parse values from repeated block and return offset thereafter
                offset = self.set_group_attribute(self.message_struct[key], offset)
            else:
                # do loop over single attribute
                self.set_single_attribute(key, self.message_struct[key], offset) # get single values
                self.single_attribs[key] = getattr(self, key)
                offset += struct.calcsize(self.message_struct[key])

        # Set the utc time if it is the correct method
        if self._class == 0x02 and self._msg_id == 0x15:
            self.utc_time = rcv_tow_2_utc(self.rcvTow, self.week,self.leapS)
            self.single_attribs['utc_time'] = self.utc_time.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]

    def set_group_attribute(self, group_attr, parent_offset):
        """
        A method to extract the values of a nested group of attributes from within a UBX message
        :param dict group_attr: dictionary of attributes
        :param int parent_offset: message offset prior to repeated block
        :return:
        """
        index = 1  # index to track which repeated block we are on
        num_blocks, attribs = group_attr # getting the number of repeated blocks key
        rng = getattr(self, num_blocks)  # number of blocks to parse
        while index <= rng:
            child_offset = 0  # offset to track what part of the payload we are extracting
            for key in attribs:
                # set each attribute within the group

                self.set_single_attribute(key, attribs[key], parent_offset + child_offset)
                if rng > 1:
                    self.grouped_attribs[key].append(getattr(self, key))  # if more than one value per attribute, pack into a list
                    delattr(self, key)
                child_offset += struct.calcsize(attribs[key])  # offset for within attribute group
            parent_offset += child_offset
            index += 1
        return parent_offset

    def set_single_attribute(self, attr, size, offset):
        """

        :param str attr: measurement name
        :param bytes size: buffer size
        :param int offset: message offset prior
        :return:
        """
        setattr(self, attr, struct.unpack_from(size, self.payload, offset)[0])

    def get_dataframe(self):

        if ~self.parsed_flag:  # check whether the data has already been parsed before creating df
            self.get_UBX_message()

        if self.single_attribs:
            result = pd.DataFrame(self.single_attribs, index=[0])
            if self.grouped_attribs:
                group_df = pd.concat({k: pd.Series(v) for k, v in self.grouped_attribs.items()}, axis=1)
                df2 = pd.concat([result]*group_df.__len__(),ignore_index=True)
                result = pd.concat([df2, group_df], axis=1)
        else:
            raise ube('There is no data avaialable to convert to a dataframe ')

        return result

